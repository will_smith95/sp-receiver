//
//  DownloadViewController.swift
//  SPReceiver
//
//  Created by Will Smith on 16/01/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import UIKit
import RHSideButtons
import SQLite
import Alamofire

//public class DownloadObject{
//
//    var Title: String = ""
//    var Count: Int = -1
//    var Progress: String = ""
//    var Downloading: Bool = false
//
//    init(Title: String = "", Count: Int = -1, Progress: String = "", Downloading: Bool = false) {
//
//        self.Title = Title
//        self.Count = Count
//        self.Progress = Progress
//        self.Downloading = Downloading
//    }
//
//    deinit {}
//}

class DownloadViewController: UIViewController, RHSideButtonsDelegate, RHSideButtonsDataSource {

    @IBOutlet weak var btn_delete: UIButton!
    
    @IBOutlet weak var btn_download: ButtonDesignable!
    
    @IBOutlet weak var txt_progress: UITextView!
    @IBOutlet weak var lbl_lastDownload: UILabel!
    @IBOutlet weak var lbl_imageCount: UILabel!
        
    var currentImages: [ImageObject] = []
    var counter: Int = 0
    
    var triggerButton: RHTriggerButtonView!
    var triggerSideView: RHSideButtons!
    var sideButtonList: [SideButtonObject] = []
    
    let df: DateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tbl_download.delegate = self
//        tbl_download.dataSource = self
//
//        tbl_download.register(UINib(nibName: "DatabaseHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "DatabaseHeader")
        
        df.timeZone = TimeZone(abbreviation: "GMT")
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        UserDefaults.standard.set(false, forKey: "DownloadInProgress")

        CheckDatabase()
    }
    
    override func didReceiveMemoryWarning() {
        
        print("MEMORY!!!!")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        configureMenu()
    }
    
    @IBAction func DownloadData(_ sender: UIButton){

        let db: Database = Database()
        
        UserDefaults.standard.set(true, forKey: "DownloadInProgress")
                    
        self.txt_progress.isHidden = false
        self.txt_progress.text = "Download Progress:\n\nPending..."
        
        let downloadQueue = DispatchQueue(label: "com.uk.pricecheck.DownloadDatabase", qos: .utility, attributes: [.concurrent])
        let url: String = AD_Authentication.PRICECHECK_DOWNLOAD
        
        var i = 1
        
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 20
        
        AD_Authentication.obtainToken(resource: AD_Authentication.CLIENT_ID){ (token, error) in

            if error == nil {
                
                Alamofire.request(url, encoding: JSONEncoding.default, headers: nil).responseJSON(queue: downloadQueue){ [unowned self] (response: DataResponse<Any>) in

                    switch(response.result) {

                    case .success(_):
                        
                        DispatchQueue.main.async{
                            
                            self.txt_progress.text = "Download Progress:\n\nAll Images Prepared For Download..."
                        }
                        
                        if self.counter > 0{
                            
                            self.currentImages = db.getRows(table: .Images) as? [ImageObject] ?? []
                                                
                            db.deleteAllRows(table: .Images)
                        }
                        
                        for obj in (response.result.value as? NSDictionary ?? [:]).value(forKey: "Images") as? NSArray ?? []{
                                                       
                            if obj as? NSDictionary ?? [:] != [:]{
                           
                                weak var prod = self.currentImages.first(where: {$0.ImageName == (obj as? NSDictionary ?? [:]).value(forKey: "imageName") as? String ?? "" && $0.DateModified == self.df.date(from: String((obj as? NSDictionary ?? [:]).value(forKey: "date_modified") as? String ?? "")) ?? Date()}) ?? ImageObject()

                                if (prod ?? ImageObject()).Image != Data(){

                                    DispatchQueue.main.async {

                                        db.addRowInLine(table: .Images, object: [prod ?? ImageObject()])

                                        if i <= ((response.result.value as? NSDictionary ?? [:]).value(forKey: "Images") as? NSArray ?? []).count {

                                            self.txt_progress.text = "Download Progress:\n\n\(i) Of \(((response.result.value as? NSDictionary ?? [:]).value(forKey: "Images") as? NSArray ?? []).count) Images Completed"
                                        }

                                        i += 1
                                    }
                                }else{

                                    let operation = DownloadImages(URLString: "https://res.cloudinary.com/pricecheck/image/upload/c_pad,h_600,w_800/\((obj as? NSDictionary ?? [:]).value(forKey: "imageName") as? String ?? "").jpg") { (imgData, error) in
                                        
                                        if error == nil{
                                            
                                            db.addRowInLine(table: .Images, object: [ImageObject(product: (obj as? NSDictionary ?? [:]).value(forKey: "product") as? String ?? "", imageName: (obj as? NSDictionary ?? [:]).value(forKey: "imageName") as? String ?? "", dateModified: self.df.date(from: String((obj as? NSDictionary ?? [:]).value(forKey: "date_modified") as? String ?? "")) ?? Date(), image: imgData ?? Data())])
                                         
                                            DispatchQueue.main.async{
                                                
                                                if i <= ((response.result.value as? NSDictionary ?? [:]).value(forKey: "Images") as? NSArray ?? []).count {
                                                    
                                                    self.txt_progress.text = "Download Progress:\n\n\(i) Of \(((response.result.value as? NSDictionary ?? [:]).value(forKey: "Images") as? NSArray ?? []).count) Images Completed"
                                                }
                                            }
                                            
                                            i += 1
                                        }else{
                                            
                                            i += 1
                                        }
                                    }
                                
                                    queue.addOperation(operation)
                                }
                            }else{
                           
                                i += 1
                            }
                        }

                        while queue.operationCount > 0{
                            
                            sleep(1)
                        }
                        
                        while i < ((response.result.value as? NSDictionary ?? [:]).value(forKey: "Images") as? NSArray ?? []).count{}

                        self.FinaliseDownload()
                        
                    case .failure(_):
                        
                        self.Failure(url: url, response: response)
                        
                        return
                    }
                }.downloadProgress { progress in

                    DispatchQueue.main.async{
                        
                        self.txt_progress.text = "Download Progress:\n\nPreparing Images: \(Float(progress.fractionCompleted * 100))%"
                    }
                }
            }else{
                
                UserDefaults.standard.set(false, forKey: "DownloadInProgress")
            }
        }
    }
    
    func Failure(url: String, response: DataResponse<Any>){
        
        UserDefaults.standard.set(false, forKey: "DownloadInProgress")
        
        if String(describing: response.result.error).contains("offline") {

            let alert = UIAlertController(title: "No Connection Found", message: "You Are Not Connected To Internet\nConnect and Try Again!", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alert.addAction(action)
            
            present(alert, animated: true, completion: nil)
        }

        if String(describing: response.result.error).contains("timeout") {

            let alert = UIAlertController(title: "Connection Timeout", message: "The Download Request TimedOut\nTry Again!", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            
            present(alert, animated: true, completion: nil)
        }else{

            let alert = UIAlertController(title: "Error Occurred", message: "The Download Request Failed -\(String(describing: response.result.error)).\nFor URL\n\(url)", preferredStyle: .alert)

            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)

            present(alert, animated: true, completion: nil)
        }
    }
    
    func FinaliseDownload(){
        
        let df: DateFormatter = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"

        UserDefaults.standard.set(df.string(from: Date()), forKey: "LastDownload")
        UserDefaults.standard.set(false, forKey: "DownloadInProgress")
        
        DispatchQueue.main.async{ [unowned self] in
            
            self.CheckDatabase()
            
            let alert = UIAlertController(title: "Processing", message: "SP Receiver Will Now Restart To Process The Data", preferredStyle: .alert)
            
            self.present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                
                alert.dismiss(animated: false, completion: nil)
                
                self.LeaveDownload()
            }
        }
    }
    
    @IBAction func DeleteData(_ sender: UIButton) {
                
        currentImages = []
        Database().deleteAllRows(table: .Images)
                 
        btn_delete.isEnabled = false
        btn_delete.backgroundColor = UIColor(colourWithHexValue: 0xB2BEC3)
                   
        btn_download.setTitle("DOWNLOAD DATA", for: .normal)
        
        UserDefaults.standard.set("N / A", forKey: "LastDownload")
        lbl_imageCount.text = "0"
        lbl_lastDownload.text = UserDefaults.standard.value(forKey: "LastDownload") as? String ?? "N / A"
    }
    
    func LeaveDownload(){
        
        dismiss(animated: true, completion: nil)
    }
    
    func configureMenu(){
        
        triggerButton = RHTriggerButtonView(pressedImage: UIImage(named: "MenuClose-Red")!) {
            
            $0.image = UIImage(named: "ReceiverMenu")
            $0.hasShadow = true
        }
        
        triggerSideView = RHSideButtons(parentView: view, triggerButton: triggerButton)
        triggerSideView.delegate = self
        triggerSideView.dataSource = self
        
        triggerSideView.setTriggerButtonPosition(CGPoint(x: view.frame.maxX - triggerButton.frame.width - 20, y: view.frame.height - triggerButton.frame.height - 10))
        
        sideButtonList.append(
            SideButtonObject(name: "Home", button: RHButtonView {
                
                $0.image = UIImage(named: "MenuHome")
                $0.hasShadow = true
            })
        )
        
        triggerSideView.reloadButtons()
    }
    
    func CheckDatabase(){
                
        counter = Int(Database().GetRowCount(statement: "SELECT COUNT(*) FROM Images"))
                
        if counter == 0{
            
            btn_delete.isEnabled = false
            btn_delete.backgroundColor = UIColor(colourWithHexValue: 0xB2BEC3)
            
            btn_download.setTitle("DOWNLOAD DATA", for: .normal)
        }else{
            
            btn_delete.isEnabled = true
            btn_delete.backgroundColor = UIColor(colourWithHexValue: 0xFF6754)
            
            btn_download.setTitle("UPDATE DATA", for: .normal)
        }
        
        lbl_lastDownload.text = UserDefaults.standard.value(forKey: "LastDownload") as? String ?? "N / A"
        lbl_imageCount.text = String(describing: counter)
    }
    
    func sideButtonsNumberOfButtons(_ sideButtons: RHSideButtons) -> Int {
        
        return sideButtonList.count
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, didSelectButtonAtIndex index: Int) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, buttonAtIndex index: Int) -> RHButtonView {
        
        return sideButtonList[index].button
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, didTriggerButtonChangeStateTo state: RHButtonState) {
        
        if state == .shown{
            
            triggerButton.hasShadow = false
        }else{
            
            triggerButton.hasShadow = true
        }
    }
}

//class DatabaseHeader: UITableViewHeaderFooterView{
//
//    @IBOutlet weak var lbl_title: UILabel!
//    @IBOutlet weak var lbl_table: UILabel!
//    @IBOutlet weak var lbl_count: UILabel!
//}
