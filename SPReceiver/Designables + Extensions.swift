//
//  Designables.swift
//  SPReceiver
//
//  Created by Will Smith on 01/02/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class ButtonDesignable: UIButton{
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColour: UIColor = UIColor.white {
        didSet{
            
            self.layer.borderColor = borderColour.cgColor
        }
    }
}

@IBDesignable class ViewDesignable: UIView{
    
    @IBInspectable var CornerRadius: CGFloat = 0{
        
        didSet{
            
            self.layer.cornerRadius = CornerRadius
        }
    }
    
    @IBInspectable var BorderWidth: CGFloat = 0{
        
        didSet{
            
            self.layer.borderWidth = BorderWidth
        }
    }
    
    @IBInspectable var BorderColour: UIColor = UIColor.white{
        
        didSet{
            
            self.layer.borderColor = BorderColour.cgColor
        }
    }
}

extension UIColor{
    
    convenience init(colourWithHexValue value: Int, alpha: CGFloat = 1.0){
        self.init(
            
            red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat((value & 0x0000FF)) / 255.0,
            alpha: alpha
        )
    }
}
