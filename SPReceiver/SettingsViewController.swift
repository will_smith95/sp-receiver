//
//  SettingsViewController.swift
//  SPReceiver
//
//  Created by Will Smith on 01/02/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import UIKit
import RHSideButtons
import Alamofire
import SwiftMessages

class SettingsViewController: UIViewController, RHSideButtonsDelegate, RHSideButtonsDataSource {

    @IBOutlet weak var txt_pricecheckUsername: UILabel!
    @IBOutlet weak var lbl_loggedIn: UILabel!
    @IBOutlet weak var btn_logInOut: UIButton!

    var triggerButton: RHTriggerButtonView!
    var triggerSideView: RHSideButtons!
    var sideButtonList: [SideButtonObject] = []
    
    var newLogin: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMenu()
        checkLogin()
    }

    @IBAction func LogInOut(_ sender: UIButton) {
        
        if btn_logInOut.title(for: .normal) == "Log In" {
            
            SwiftMessages.hide()
            GetSettings(refresh: false)
        }else{
            
            let al = UIAlertController(title: "Log Out", message: "Are You Sure You Want To Log Out?", preferredStyle: .alert)
            
            let yes = UIAlertAction(title: "Yes", style: .destructive, handler: { [unowned self] action in
                
                AD_Authentication.logout()
                UserDefaults.standard.set([:], forKey: "Permissions")
                
                self.checkLogin()
            })
            
            let no = UIAlertAction(title: "No", style: .default, handler: nil)
            
            al.addAction(yes)
            al.addAction(no)
            
            present(al, animated: true, completion: nil)
        }
    }
    
    func GetSettings(refresh: Bool){
        
        let login = DispatchQueue(label: "com.uk.pricecheck.login", attributes: [.concurrent])
        
        var isComplete = false
        var newError: Error?
        
        login.async {
            
            AD_Authentication.getPricecheckSettings(completion: { [unowned self] (settings, error) in
                
                if error == nil {
                    
                    UserDefaults.standard.set(settings, forKey: "Permissions")
                    
                    self.newLogin = true
                    isComplete = true
                }else{
                    
                    UserDefaults.standard.set([:], forKey: "Permissions")
                    self.newLogin = false
                    
                    newError = error!
                    isComplete = true
                }
                
                while isComplete == false {}
                
                if isComplete {
                    
                    DispatchQueue.main.async{
                        
                        self.checkLogin(refresh: refresh, error: newError)
                    }
                }
            })
        }
    }
    
    func checkLogin(refresh: Bool = false, error: Error? = nil){
        
        if UserDefaults.standard.object(forKey: "Permissions") as? NSDictionary ?? [:] != [:] {
            
            let username = (UserDefaults.standard.value(forKey: "Permissions") as? NSDictionary ?? [:]).value(forKey: "Username") as? String ?? ""
            
            txt_pricecheckUsername.text = username
            lbl_loggedIn.text = "You Are Logged Into Sales Presenter"
            btn_logInOut.setTitle("Log Out", for: .normal)
            
            txt_pricecheckUsername.isHidden = false
            
            if newLogin {
                
                var config = SwiftMessages.defaultConfig
                config.duration = .seconds(seconds: 3)
                
                let view = Globals.MessageTemplate()
                Globals.ConfigureMessage(refresh: refresh, view: view, user: username)
                
                SwiftMessages.show(config: config, view: view)
            }
        }else{
            
            if error != nil{
                
                var config = SwiftMessages.defaultConfig
                config.duration = .forever
                
                let view = Globals.MessageTemplate()
                Globals.ConfigureMessage(refresh: refresh, error: error!, view: view)
                
                SwiftMessages.show(config: config, view: view)
            }
            
            lbl_loggedIn.text = "You Are Not Logged In\nThis Will Limit App Functionality"
            btn_logInOut.setTitle("Log In", for: .normal)
            
            txt_pricecheckUsername.isHidden = true
        }
    }
    
    func configureMenu(){
        
        triggerButton = RHTriggerButtonView(pressedImage: UIImage(named: "MenuClose-Red")!) {
            
            $0.image = UIImage(named: "ReceiverMenu")
            $0.hasShadow = true
        }
        
        triggerSideView = RHSideButtons(parentView: view, triggerButton: triggerButton)
        triggerSideView.delegate = self
        triggerSideView.dataSource = self
        
        triggerSideView.setTriggerButtonPosition(CGPoint(x: view.frame.maxX - triggerButton.frame.width - 20, y: view.frame.height - triggerButton.frame.height - 10))
        
        sideButtonList.append(
            SideButtonObject(name: "Home", button: RHButtonView {
                
                $0.image = UIImage(named: "MenuHome")
                $0.hasShadow = true
            })
        )
        
        triggerSideView.reloadButtons()
    }
    
    func sideButtonsNumberOfButtons(_ sideButtons: RHSideButtons) -> Int {
        
        return sideButtonList.count
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, didSelectButtonAtIndex index: Int) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, buttonAtIndex index: Int) -> RHButtonView {
        
        return sideButtonList[index].button
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, didTriggerButtonChangeStateTo state: RHButtonState) {
        
        if state == .shown{
            
            triggerButton.hasShadow = false
        }else{
            
            triggerButton.hasShadow = true
        }
    }
}
