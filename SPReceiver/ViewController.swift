//
//  ViewController.swift
//  SPReceiver
//
//  Created by Will Smith on 11/01/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import UIKit
import BluetoothKit
import CoreBluetooth
import SwiftMessages
import RHSideButtons
import SQLite

struct SideButtonObject{
    
    var name: String = ""
    var button: RHButtonView!
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, BKPeripheralDelegate, CBCentralManagerDelegate, BKRemotePeripheralDelegate, BKAvailabilityObserver, BKRemotePeerDelegate, RHSideButtonsDelegate, RHSideButtonsDataSource {
    
    func remotePeer(_ remotePeer: BKRemotePeer, didSendArbitraryData data: Data) {
        
        var response: Dictionary<String, Dictionary<String, Dictionary<String, String>>> = [:]
        
        do {
            
            response = try JSONDecoder().decode(Dictionary<String, Dictionary<String, Dictionary<String, String>>>.self, from: data)
        }catch{
            
            print("NOT DECODABLE")
            return
        }
        
        currentProduct = ""
        currentDescription = ""
        
        if response != [:] {

            currentProduct = ((response["Product"] ?? [:])["10"] as NSDictionary? ?? [:])?.value(forKey: "Product") as? String ?? "* No Product *"
            
            currentDescription = ((response["Product"] ?? [:])["11"] as NSDictionary? ?? [:])?.value(forKey: "Description") as? String ?? "* No Description *"
            
            response["Product"]?.removeValue(forKey: "10")
            response["Product"]?.removeValue(forKey: "11")
            
            if response["Product"] ?? [:] != [:]{
                
                productInfo = response["Product"] ?? [:]
                priceInfo = response["Price"] ?? [:]
                
                lbl_product.text = "\(currentProduct) - \(currentDescription)"
                
                images = Database().getRows(table: .Images, filter: Expression<Bool>(Database().im_product == currentProduct)) as? [ImageObject] ?? []
                        
                images.sort(by: {$0.ImageName < $1.ImageName})
                
                if images.count > 0{
                    
                    img_product.image = UIImage(data: images[0].Image) ?? UIImage(named: "noImage.jpg")!
                }else{
                    
                    img_product.image = UIImage(named: "noImage.jpg")!
                }
            }
            
            orderInfo = response["Order"] ?? [:]
            
            exchangeRate = ((response["Currency"] as NSDictionary? ?? [:]).value(forKey: "CurrentExchange") as? NSDictionary ?? [:]).value(forKey: "Rate") as? String ?? "1"
            
            exchangeSymbol = ((response["Currency"] as NSDictionary? ?? [:]).value(forKey: "CurrentExchange") as? NSDictionary ?? [:]).value(forKey: "Symbol") as? String ?? "£"
            
            if ((response["CustomSettings"] as NSDictionary? ?? [:]).value(forKey: "Settings") as? NSDictionary ?? [:]).value(forKey: "Show Totals") as? String ?? "NO" == "YES"{
                
                lbl_orderVal.isHidden = false
                lbl_orderValueTotal.isHidden = false
            }else{
                
                lbl_orderVal.isHidden = true
                lbl_orderValueTotal.isHidden = true
            }
            
            if ((response["CustomSettings"] as NSDictionary? ?? [:]).value(forKey: "Settings") as? NSDictionary ?? [:]).value(forKey: "Show Pallets") as? String ?? "NO" == "YES"{
                
                lbl_pallet.isHidden = false
                lbl_palletCount.isHidden = false
            }else{
                
                lbl_pallet.isHidden = true
                lbl_palletCount.isHidden = true
            }
        }
        
        CalculateOrderValues()
        
        tbl_information.reloadData()
        tbl_order.reloadData()
        tbl_price.reloadData()
        col_images.reloadData()
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if !CheckBluetooth(){
            
            ConfigMessage(status: "Unavailable")
        }
    }
    
    func remotePeripheral(_ remotePeripheral: BKRemotePeripheral, didUpdateName name: String) {}
    
    func remotePeripheralIsReady(_ remotePeripheral: BKRemotePeripheral) {}
    
    func availabilityObserver(_ availabilityObservable: BKAvailabilityObservable, availabilityDidChange availability: BKAvailability) {}
    
    func availabilityObserver(_ availabilityObservable: BKAvailabilityObservable, unavailabilityCauseDidChange unavailabilityCause: BKUnavailabilityCause) {}
    
    //@IBOutlet weak var txt_bluetooth: UITextView!
    //@IBOutlet weak var txt_dataLog: UITextView!
    @IBOutlet weak var img_product: UIImageView!
    
    @IBOutlet weak var tbl_information: UITableView!
    @IBOutlet weak var tbl_price: UITableView!
    @IBOutlet weak var tbl_order: UITableView!
    
    @IBOutlet weak var scrollView_img: UIScrollView!
    
    @IBOutlet weak var lbl_product: UILabel!
    @IBOutlet weak var lbl_currentOrder: UILabel!
    @IBOutlet weak var lbl_orderVal: UILabel!
    @IBOutlet weak var lbl_orderValueTotal: UILabel!
    @IBOutlet weak var lbl_line: UILabel!
    @IBOutlet weak var lbl_lineCount: UILabel!
    @IBOutlet weak var lbl_pallet: UILabel!
    @IBOutlet weak var lbl_palletCount: UILabel!
    @IBOutlet weak var col_images: UICollectionView!
    
    var peripheral = BKPeripheral()
    var RemotePeripheral: BKRemotePeripheral!
    
    let db: Database = Database()
    var central: CBCentralManager!
    var realPeripheral: CBPeripheralManager!
    
    var triggerButton: RHTriggerButtonView!
    var sideButtonsView: RHSideButtons!
    var sideButtonList: [SideButtonObject] = []
    
    var nf: NumberFormatter = NumberFormatter()
    
    var name: String = ""
    
    var newLogin: Bool = false
    
    let df = DateFormatter()
    
    var images: [ImageObject] = []
    
    var currentProduct: String = ""
    var currentDescription: String = ""
    
    var exchangeRate: String = "1"
    var exchangeSymbol: String = "£"
    
    var productInfo: Dictionary<String, Dictionary<String, String>> = [:]
    var orderInfo: Dictionary<String, Dictionary<String, String>> = [:]
    
    var priceInfo: Dictionary<String, Dictionary<String, String>> = [:]
    
    func defaultDate() -> Date{
        
        let date: Date = df.date(from: "01/01/1900")!
        
        return date
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()

        df.dateFormat = "dd/MM/yyyy"
        
        peripheral.delegate = self
        peripheral.addAvailabilityObserver(self)
        
        tbl_price.delegate = self
        tbl_price.dataSource = self
        
        tbl_order.delegate = self
        tbl_order.dataSource = self
        
        tbl_information.delegate = self
        tbl_information.dataSource = self
        
        name = UIDevice.current.name
        
        configureMenu()

        col_images.delegate = self
        col_images.dataSource = self
        
        scrollView_img.delegate = self
        
        scrollView_img.minimumZoomScale = 1
        scrollView_img.maximumZoomScale = 6
        
        nf.numberStyle = .decimal
        nf.maximumFractionDigits = 2
        nf.minimumFractionDigits = 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ImageColCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! ImageColCell
        
        cell.img_product.image = UIImage(data: images[indexPath.row].Image) ?? UIImage(named: "noImage.jpg")!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        img_product.image = UIImage(data: images[indexPath.row].Image) ?? UIImage(named: "noImage.jpg")!
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return img_product
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count: Int = 0
        
        if tableView == tbl_order{
            
            count = orderInfo.count
        }else if tableView == tbl_information{
            
            count = productInfo.count
        }else{
            
            count = priceInfo.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tbl_order{
            
            let cell: OrderCell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
            
            let product: String = ((orderInfo as NSDictionary? ?? [:]).allKeys)[indexPath.row] as? String ?? ""
            
            let images: [ImageObject] = Database().getRows(table: .Images, filter: Expression<Bool>(Database().im_product == product && Database().im_imageName.like("%-1%"))) as? [ImageObject] ?? []
            
            if images.count > 0{
                
                cell.img_image.image = UIImage(data: images[0].Image) ?? UIImage(named: "noImage.jpg")!
            }else{
                
                cell.img_image.image = UIImage(named: "noImage.jpg")!
            }
            
            cell.lbl_prod.text = "\(product)"
            
            cell.lbl_description.text = "\((orderInfo[product] as NSDictionary? ?? [:]).value(forKey: "Description") as? String ?? "* Description *")"
            
            cell.lbl_price.text = "\(exchangeSymbol)\((orderInfo[product] as NSDictionary? ?? [:]).value(forKey: "Price") as? String ?? "N/A")"
            
            cell.lbl_quantity.text = (orderInfo[product] as NSDictionary? ?? [:]).value(forKey: "Quantity") as? String ?? "N/A"
            
            cell.lbl_value.text = "\(exchangeSymbol)\((orderInfo[product] as NSDictionary? ?? [:]).value(forKey: "Value") as? String ?? "N/A")"
            
            return cell
        }else if tableView == tbl_information{
            
            let cell: DataCell = tableView.dequeueReusableCell(withIdentifier: "CurrentProductCell", for: indexPath) as! DataCell
                        
            for obj in productInfo["\(indexPath.row)"] as NSDictionary? ?? [:] {
                
                cell.lbl_Field.text = obj.key as? String ?? "Key"
                
                if obj.value as? String ?? "" == ""{
                    
                    cell.lbl_Value.text = "N/A"
                }else{
                    
                    cell.lbl_Value.text = obj.value as? String ?? "N/A"
                }
            }
            
            return cell
        }else{
            
            let cell: DataCell = tableView.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath) as! DataCell
            
            for obj in priceInfo["\(indexPath.row)"] as NSDictionary? ?? [:] {
                
                cell.lbl_Field.text = obj.key as? String ?? "Key"
                
                cell.lbl_Value.text = "\(exchangeSymbol)\(obj.value as? String ?? "N/A")"
            }
            
            return cell
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Login()
    }
    
    func FailedLogin(error: Error?){

        var config = SwiftMessages.defaultConfig
        config.duration = .forever

        let view = Globals.MessageTemplate()
        
        view.button?.setTitle("Settings", for: .normal)
        view.button?.addTarget(self, action: #selector(GoLocalSettings), for: .touchUpInside)
        view.button?.layer.cornerRadius = 6
        view.button?.isHidden = false
        
        Globals.ConfigureMessage(error: error ?? nil, view: view)
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func CalculateOrderValues(){
        
        var orderValue: Double = 0
        var palletCount: String = "0"
        
        for obj in orderInfo{
            
            let prodDict: NSDictionary = obj.value as NSDictionary? ?? [:]
            
            orderValue += Double((prodDict.value(forKey: "Value") as? String ?? "0").replacingOccurrences(of: ",", with: "")) ?? 0
            
            palletCount = prodDict.value(forKey: "Pallets") as? String ?? "< 1"
        }
        
        lbl_orderVal.text = "Order Value (\(exchangeSymbol)):"
        
        lbl_orderValueTotal.text = nf.string(for: orderValue) ?? "0"
        lbl_lineCount.text = "\(orderInfo.count)"
        lbl_palletCount.text = palletCount
    }
    
    @objc func GoLocalSettings(){
        
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = sb.instantiateViewController(withIdentifier: "SettingsNav")
        
        do {
            
            try peripheral.stop()
        }catch let error{
            
            print(error)
        }
        
        SwiftMessages.hideAll()
        
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func unwindHome(segue: UIStoryboardSegue) {}
    
    func configureMenu(){
        
        triggerButton = RHTriggerButtonView(pressedImage: UIImage(named: "MenuClose-Red")!) {
            
            $0.image = UIImage(named: "ReceiverMenu")
            $0.hasShadow = true
        }
        
        sideButtonsView = RHSideButtons(parentView: view, triggerButton: triggerButton)
        
        sideButtonsView.delegate = self
        sideButtonsView.dataSource = self
        
        sideButtonsView.setTriggerButtonPosition(CGPoint(x: view.frame.maxX - triggerButton.frame.width - 20, y: view.frame.height - triggerButton.frame.height - 10))
        
        sideButtonList.append(
            SideButtonObject(name: "Settings", button: RHButtonView {
                
                $0.image = UIImage(named: "MenuSettings")
                $0.hasShadow = true
            })
        )
        
        sideButtonList.append(
            SideButtonObject(name: "Download", button: RHButtonView {
                
                $0.image = UIImage(named: "MenuDownload")
                $0.hasShadow = true
            })
        )
        
        sideButtonsView.reloadButtons()
    }
    
    func sideButtonsNumberOfButtons(_ sideButtons: RHSideButtons) -> Int {
        
        return sideButtonList.count
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, buttonAtIndex index: Int) -> RHButtonView {
     
        return sideButtonList[index].button
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, didSelectButtonAtIndex index: Int) {
        
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "\(sideButtonList[index].name)Nav")
        
        do {
            
            try peripheral.stop()
        }catch let error{
            
            print(error)
        }
        
        SwiftMessages.hideAll()
        
        vc.modalPresentationStyle = .fullScreen
        
        //UserDefaults.standard.set(true, forKey: "FirstLoad")
        present(vc, animated: true, completion: nil)
        
        //Navigate(page: sideButtonList[index].name)
    }
    
    func sideButtons(_ sideButtons: RHSideButtons, didTriggerButtonChangeStateTo state: RHButtonState) {
        
        if state == .shown{
            
            triggerButton.hasShadow = false
        }else{
            
            triggerButton.hasShadow = true
        }
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//
//        if UserDefaults.standard.value(forKey: "FirstLoad") as? Bool ?? true{
//
//            UserDefaults.standard.set(false, forKey: "FirstLoad")
//            Login()
//        }
//    }
    
    func Login() {
        
        var isLoggedIn = false
        
        if (UserDefaults.standard.object(forKey: "Permissions") as? NSDictionary ?? [:]) == [:] {
            
            let login = DispatchQueue(label: "com.uk.pricecheck.login", attributes: [.concurrent])
                        
            login.async {
                
                AD_Authentication.getPricecheckSettings(completion: {(settings, error) in
                    
                    if error == nil {
                        
                        if settings?.object(forKey: "Message") as? String ?? "" != ""{
                            
                            UserDefaults.standard.set([:], forKey: "Permissions")
                        }else{
                            
                            UserDefaults.standard.set(settings, forKey: "Permissions")
                            
                            var config = SwiftMessages.defaultConfig
                            config.duration = .seconds(seconds: 3)
                            
                            let username = (settings ?? [:]).value(forKey: "Username") as? String ?? ""
                            
                            let view = Globals.MessageTemplate()
                            Globals.ConfigureMessage(view: view, user: username)
                            
                            SwiftMessages.show(config: config, view: view)
                        }
                        
                        isLoggedIn = true
                    }else{
                        
                        UserDefaults.standard.set([:], forKey: "Permissions")
                        isLoggedIn = true
                    }
                    
                    
                    while isLoggedIn == false {}
                    
                    if UserDefaults.standard.value(forKey: "Permissions") as? NSDictionary ?? [:] != [:] {
                        
                        DispatchQueue.main.async { [unowned self] in
                            
                            self.AppearConfig()
                        }
                    }else{
                        
                        DispatchQueue.main.async { [unowned self] in
                            
                            self.FailedLogin(error: error)
                        }
                    }
                })
            }
        }else{
            
            AppearConfig()
        }
    }
    
    func AppearConfig(){
        
        central = CBCentralManager(delegate: self, queue: nil)
        
        if UserDefaults.standard.value(forKey: "LastDownload") as? String ?? "N / A" == "N / A"{
            
            ConfigMessage(status: "DownloadData")
        }else{
            
//            images = Database().getRows(table: .Images) as? [ImageObject] ?? []
//
//            images.sort(by: {$0.Product < $1.Product})
            
            let today: Date = Date()
            let downloadDateString: String = UserDefaults.standard.value(forKey: "LastDownload") as? String ?? "N / A"
            
            let downloadDate: Date = df.date(from: downloadDateString) ?? defaultDate()
            
            let compareDate: Date = Calendar.current.date(byAdding: .day, value: 1, to: downloadDate)!
            
            if (downloadDate != defaultDate() && compareDate < today){
                
                ConfigMessage(status: "UpdateData")
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: { [unowned self] in
                
                self.InitialiseBluetooth()
            })
        }
    }
    
    func ConfigMessage(status: String){
        
        var title: String = ""
        var content: String = ""
        var icon: String = "🌎"
        var theme: Theme = .info
        
        var config = SwiftMessages.defaultConfig
        config.duration = .seconds(seconds: 3)
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureDropShadow()
        view.button?.isHidden = true
        
        switch status{
            
        case "Unavailable":
            
            title = "Bluetooth Unavailable"
            content = "Sales Presenter Requires Bluetooth In Order To Present Screen"
            
            theme = .info
            break
        case "DownloadData":
            
            title = "No Data"
            content = "There is no data downloaded!\nYou will need to download data before connecting to this device"
            icon = "❌"
            
            view.button?.setTitle("Download Now", for: .normal)
            view.button?.addTarget(self, action: #selector(GoDownload), for: .touchUpInside)
            view.button?.isHidden = false
            
            config.duration = .forever
            
            theme = .warning
            break
        case "UpdateData":
            
            title = "Old Data"
            content = "You haven't updated data today!\nYou may need an update to keep in sync with Sales Presenter"
            icon = "⚠️"
            
            view.button?.setTitle("Update Data", for: .normal)
            view.button?.addTarget(self, action: #selector(GoDownload), for: .touchUpInside)
            view.button?.isHidden = false
            
            theme = .info
            break
        case "Connected":
            
            title = "Bluetooth Connected"
            content = "You Are Connected To Sales Presenter"
            icon = "🌐"
            
            view.button?.isHidden = true
            
            theme = .success
            break
        case "Disconnected":
            
            title = "Bluetooth Disconnected"
            content = "Lost Connection With Sales Presenter!"
            icon = "🌐"
            view.button?.isHidden = true
            
            theme = .error
            break
        default:
            break
        }
        
        view.configureTheme(theme)
        view.configureContent(title: title, body: content, iconText: icon)

        SwiftMessages.show(config: config, view: view)
    }
    
    @objc func GoDownload(){
        
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "DownloadNav")
        
        do {

            try peripheral.stop()
        }catch let error{

            print(error)
        }
        
        SwiftMessages.hideAll()
        
        vc.modalPresentationStyle = .fullScreen

        present(vc, animated: true, completion: nil)
        //performSegue(withIdentifier: "DownloadSegue", sender: self)
    }
    
//    func Navigate(page: String){
//        
//        do {
//            
//            try peripheral.stop()
//        }catch let error{
//            
//            print(error)
//        }
//        
//        SwiftMessages.hideAll()
//        performSegue(withIdentifier: "\(page)Segue", sender: self)
//    }
    
    func GetState(current: CBManagerState) -> String{
        
        var state: String = ""
        
        switch current {
            
            case .poweredOff:
                
                state = "Powered Off"
                break
            case .poweredOn:
                
                state = "Powered On"
                break
            case .resetting:
                
                state = "Resetting"
                break
            case .unauthorized:
                
                state = "Unauthorised"
                break
            case .unknown:
                
                state = "Unknown"
                break
            case .unsupported:
                
                state = "Unsupported"
                break
            default:
                break
            }
        
        return state
    }
    
    func CheckBluetooth() -> Bool{
        
        if central.state != .poweredOn {
            
            ConfigMessage(status: "Unavailable")
            
            return false
        }else{
            
            return true
        }
    }
    
    @objc func GoSettings(){
        
        UIApplication.shared.open(URL(string: "App-prefs:root=Bluetooth")!, options: [:], completionHandler: nil)
    }
    
    func InitialiseBluetooth(){
        
        if CheckBluetooth() {

            do {

                let serviceUUID = UUID(uuidString: "666EEFEF-01C2-4C41-8BE6-A2256CD7BEC4")!

                let characteristicUUID = UUID(uuidString: "0055D05F-4096-4898-831C-E3359E1EA414")!

                let localName = "SP Receiver - \(name)"

                let configuration = BKPeripheralConfiguration(dataServiceUUID: serviceUUID, dataServiceCharacteristicUUID: characteristicUUID, localName: localName)

                try peripheral.startWithConfiguration(configuration)
            }catch let error{

                print("Error - Setting Device Name!\n\n\(error.localizedDescription)")
            }
        }
    }
    
    func peripheral(_ peripheral: BKPeripheral, remoteCentralDidConnect remoteCentral: BKRemoteCentral) {

        //txt_dataLog.text = "Welcome To Sales Presenter Receiver!"
        
        //let df = DateFormatter()
        //df.timeStyle = .medium
        
        //let dateString = df.string(from: Date())
        
        //txt_dataLog.text += "\nConnected To Sales Presenter [\(dateString)]"
        ConfigMessage(status: "Connected")
        
        remoteCentral.delegate = self
        
        peripheral.sendData("SP Receiver - \(name)".data(using: String.Encoding.utf8)!, toRemotePeer: remoteCentral) { (data, remote, error) in

            if error != nil{

                print("ERROR")
                print(error?.localizedDescription ?? "ERROR - ???")
            }
        }
        
        triggerButton.isHidden = true
    }

    func peripheral(_ peripheral: BKPeripheral, remoteCentralDidDisconnect remoteCentral: BKRemoteCentral) {

        //let df = DateFormatter()
        
        //df.timeStyle = .medium
        
        //let dateString = df.string(from: Date())
        
       // if !txt_dataLog.text.contains("Disconnected From Sales Presenter"){
            
            //txt_dataLog.text += "\nDisconnected From Sales Presenter [\(dateString)]"
            ////img_product.image = nil
        //}
        
        ConfigMessage(status: "Disconnected")
        
        triggerButton.isHidden = false
    }
}

class Globals {
    
    static func MessageTemplate() -> MessageView{
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureDropShadow()
        view.button?.isHidden = true
        
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 5
        
        return view
    }
    
    static func ConfigureMessage(refresh: Bool = false, error: Error? = nil, view: MessageView, user: String? = ""){
        
        if error == nil && user != ""{
            
            var title: String = "Signed In"
            var message: String = "Welcome To SP Receiver, " + user!
            
            if refresh{
                
                title = "Settings Refreshed"
                message = "Your User Settings Have Been Refreshed!"
            }
            
            view.configureTheme(.success)
            view.configureContent(title: title, body: message, iconImage: UIImage(named: "WhiteLogoSuccess.png")!)//"🌍")
        }else{
            
            let iconText: Dictionary<String, Any> = ["Cancel": #imageLiteral(resourceName: "Warning"), "Unauthorised": #imageLiteral(resourceName: "Unauthorised"), "Offline": "🌐", "Error": #imageLiteral(resourceName: "Error40")]
            
            view.configureTheme(.error)
            
            if error.debugDescription.contains("USER_CANCEL"){
                
                view.configureTheme(.warning)
                
                view.configureContent(title: "Not Signed In", body: "SP Receiver Will Limit Functionality Until You're Signed In!", iconImage: iconText["Cancel"]! as! UIImage)
            }else if error.debugDescription.contains("AUTHORIZATION_CODE"){
                
                view.configureContent(title: "Unauthorised User", body: "You Are Not Authorised To Use SP Receiver!", iconImage: iconText["Unauthorised"]! as! UIImage)
            }else if error.debugDescription.contains("offline"){
                
                view.configureContent(title: "Offline", body: "You Do Not Have An Internet Connection\nReconnect To Sign In!", iconText: iconText["Offline"]! as! String)
            }else{
                
                view.configureContent(title: "Error", body: "SP Receiver Cannot Retrieve User Information, Try Again In Settings", iconImage: iconText["Error"]! as! UIImage)
            }
        }
    }
}

class OrderCell: UITableViewCell{
    
    @IBOutlet weak var img_image: UIImageView!
    @IBOutlet weak var lbl_prod: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_quantity: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_value: UILabel!
}

class DataCell: UITableViewCell{
    
    @IBOutlet weak var lbl_Field: UILabel!
    @IBOutlet weak var lbl_Value: UILabel!
}

class ImageColCell: UICollectionViewCell{
    
    @IBOutlet weak var img_product: UIImageView!
}
