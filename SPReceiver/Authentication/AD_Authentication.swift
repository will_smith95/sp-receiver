//
//  AD_Authentication.swift
//  SalesPresenter
//
//  Created by Will Smith on 12/09/2018.
//  Copyright © 2018 Pricecheck. All rights reserved.
//

import Foundation
import ADAL
import Alamofire

class AD_Authentication {
    
    private static let AUTHORITY_URL = "https://login.microsoftonline.com/common" // COMMON/TENANT ID
    public static let CLIENT_ID = "ae1cfa27-b93b-415d-9a8d-d0d387f79222"
    
    public static let REDIRECT_URI = "http://localhost"
    public static let SERVICE_URL = "https://login.microsoftonline.com/common/oauth2/v2.0/token"
    public static let GRAPH_RESOURCE = "https://outlook.office.com"
    
    public static let PRICECHECK_USER_SETTINGS = "https://api.pricecheck.uk.com:7003/api/settings"
    
    public static let PRICECHECK_DOWNLOAD = "https://api.pricecheck.uk.com:7003/api/SPDownload/"
    
    private static let df: DateFormatter = DateFormatter()
    
    public static func InitialiseADAL(){
        
        ADAuthenticationSettings.sharedInstance().setDefaultKeychainGroup(nil)
    }
    
    private static func acquireToken(silent: Bool, resource: String, completion: @escaping (_ accessToken: String?, _ error: NSError?) -> Void) {
        
        if let _authContext = ADAuthenticationContext(authority: AUTHORITY_URL, error: nil) {
            
            if silent {
                
                _authContext.acquireTokenSilent(withResource: resource, clientId: self.CLIENT_ID, redirectUri: NSURL(string: self.REDIRECT_URI)! as URL, completionBlock: {(result) in
                    
                    if (result!.status != AD_SUCCEEDED) {
                        
                        completion(nil, result?.error)
                    }else{
                        
                        print(result!.accessToken)
                        completion(result?.accessToken, nil)
                    }
                })
            }else{
                
                _authContext.acquireToken(withResource: resource, clientId: self.CLIENT_ID, redirectUri: NSURL(string: self.REDIRECT_URI)! as URL, completionBlock: {(result) in
                    
                    if (result!.status != AD_SUCCEEDED) {
                        
                        completion(nil, result!.error)
                    }else{
                        
                        print(result!.accessToken!)
                        completion(result?.accessToken, nil)
                    }
                })
            }
        }
    }
    
    static func logout(){
        
        ADKeychainTokenCache.defaultKeychain().removeAll(forClientId: self.CLIENT_ID, error: nil)
        
        let cookieJar = HTTPCookieStorage.shared
        guard let cookies = cookieJar.cookies else{
            
            print("Cookie Jar Error")
            return
        }
        
        let cookiesArr = Array(cookies)
        
        for cookie: HTTPCookie in cookiesArr {
            
            print(cookie.name)
            
            if (cookie.name == "SignInStateCookie" || cookie.name == "ESTSAUTHPERSISTENT" || cookie.name == "ESTSAUTHLIGHT" || cookie.name == "ESTSAUTH" || cookie.name == "ESTSSC") {
                
                cookieJar.deleteCookie(cookie)
            }
        }
    }
    
    static func getPricecheckSettings(completion: @escaping (_ value: NSDictionary?, _ error: Error?) -> Void){
        
        self.obtainToken(resource: self.CLIENT_ID, completion: {(accessToken, error) in
            
            if error == nil {
                
                let token: String = accessToken ?? ""
                let h: HTTPHeaders = ["Authorization": "Bearer " + token]
                let url: String = self.PRICECHECK_USER_SETTINGS
                
                Alamofire.request(url, encoding: JSONEncoding.default, headers: h).responseJSON{ (response:DataResponse<Any>) in
                    switch(response.result){
                        
                    case .failure(_):
                        
                        completion(nil, response.error)
                    case .success(_):
                        
                        print(response)
                        if let data = response.result.value {
                            
                            guard let settings: NSDictionary = data as? NSDictionary else{
                                
                                completion(nil, NSError(domain: "", code: 0001, userInfo: ["Error": "FAIL"]))
                                return
                            }
                            
                            if (settings.value(forKey: "Username") as? String ?? "").contains("Error") {
                                
                                completion(nil, NSError(domain: "", code: 0001, userInfo: ["Error": settings.value(forKey: "Rep") as? String ?? ""]))
                            }else{
                                
                                completion(settings, nil)
                            }
                        }
                    }
                }
            }else{
                
                completion(nil, error)
            }
        })
    }
    
    static func obtainToken(resource: String, completion: @escaping (_ accessToken: String?, _ error: NSError?) -> Void){
        
        self.acquireToken(silent: true, resource: resource, completion: {(accessToken, error) in
            if error == nil {
                
                completion(accessToken, nil)
            }else{
                if let _error = error {
                    
                    if _error.code == 200 {
                        
                        DispatchQueue.main.async {
                            
                            self.acquireToken(silent: false, resource: resource, completion: {(accessToken, error) in
                                completion(accessToken, error);
                            })
                        }
                    }else{ completion("", _error) }
                }else{
                    
                    let er = NSError(domain: "Invalid error. Contact IT", code: 666, userInfo: ["Error":"Error"])
                    
                    completion("", er)
                }
            }
        })
    }
}
