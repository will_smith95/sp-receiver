//
//  Database.swift
//  SPReceiver
//
//  Created by Will Smith on 16/01/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import Foundation
import SQLite

enum DBTables: String{
    
    case Images
}

class Database {
    
    private var db: Connection?
    
    fileprivate let concurrentDBQueue = DispatchQueue(label: "com.uk.pricecheck.databaseQueue", attributes: .concurrent)
    
    //MARK: Table Definitions
    private let images = Table("Images")
    
    //MARK: Images
    let im_product = Expression<String?>("product")
    let im_imageName = Expression<String?>("imageName")
    let im_dateModified = Expression<Date?>("dateModified")
    let im_image = Expression<Data?>("image")

    init(){
        
        let fileManager = FileManager()
        let path = fileManager.urls(for: .libraryDirectory, in: .allDomainsMask)[0]
        
        do {
            
            db = try Connection("\(path)/SPReceiver.sqlite3")
        
            db!.busyTimeout = 5
            db!.busyHandler({ tries in
                
                if tries >= 3 {
                    
                    return false
                }
                    
                print("Waiting For DB To Unlock")
                return true
            })
            
            CreateTables()
        }catch{
            
            print("DB Error")
            return
        }
    }
    
    deinit {}
    
    func getTable(table: String) -> Table{
        
        switch table{
            
            case "Images":
                return self.images
            
            default:
                return Table("")
        }
    }
    
    func CreateTables(){
                
        do {
            
            try db!.transaction{ [unowned self] in
                                    
                try self.db!.run(self.images.create(ifNotExists: true) { table in
                    
                    table.column(self.im_product)
                    table.column(self.im_imageName)
                    table.column(self.im_dateModified)
                    table.column(self.im_image)
                })
            }
        }catch{
            
            print("Tables Exist")
        }
    }
    
    func addRow(table: DBTables, object: [Any]){
        
        concurrentDBQueue.async(flags: .barrier) {
            
            do {
                
                try self.db!.transaction(){ [unowned self] in
                    
                    for obj in object{
                        
                       if table.rawValue == DBTables.Images.rawValue{
                            
                            let im: ImageObject = obj as! ImageObject
                            
                            try self.db!.run(self.images.insert(
                                
                                self.im_product <- im.Product,
                                self.im_imageName <- im.ImageName,
                                self.im_dateModified <- im.DateModified,
                                self.im_image <- im.Image
                            ))
                        }
                    }
                }
            } catch let error {
                
                print("Insert failed \(error)")
            }
        }
    }
    
    func addRowInLine(table: DBTables, object: [Any]) {
        
        do {
            
            for obj in object{
                
                if table.rawValue == DBTables.Images.rawValue{
                    
                    let im: ImageObject = obj as! ImageObject
                    
                    try self.db!.run(self.images.insert(
                        
                        self.im_product <- im.Product,
                        self.im_imageName <- im.ImageName,
                        self.im_dateModified <- im.DateModified,
                        self.im_image <- im.Image
                    ))
                }
            }
        }catch let error{
            
            print(error)
        }
    }
    
    func GetRowCount(statement: String) -> Int64 {
        
        var val: Int64 = 0
        
        do{
            
            val = try db?.scalar(statement) as? Int64 ?? 0
        }catch{
            
            return 0
        }
        
        return val
    }
    
    func UpdateDateModified(imageName: String, dateModi: Date){
        
        do{
            
            let image = self.images.filter(self.im_imageName == imageName)
            try db?.run(image.update(self.im_dateModified <- dateModi))
        }catch{
            
            print("\(error)")
        }
    }
    
    func deleteRow(table: DBTables, filter: Expression<Bool>) -> Bool {
        
        var thetable: Table
        
        do {
            
            if (tableList()).contains(table.rawValue){
                
                thetable = self.getTable(table: table.rawValue)
                let obj = thetable.filter(filter)
                try db!.run(obj.delete())
                
                return true
            }
        }catch{
            
            print("Delete Failed")
        }
        
        return false
    }
    
    func deleteAllRows(table: DBTables) {
        
        concurrentDBQueue.async(flags: .barrier){
            
            do {
                
                let obj = self.getTable(table: table.rawValue)
                
                try self.db!.run(obj.delete())
            } catch let error{
                
                print("Delete Failed")
                print(error)
            }
        }
    }
    
    func getRows(table: DBTables, filter: Expression<Bool>? = nil, orderBy: Expression<String>? = nil) -> [Any] {
        
        var thetable: Table
        var object: [Any] = []
        
        if (tableList()).contains(table.rawValue){
            
            thetable=self.getTable(table: table.rawValue)
            
            if filter == nil{}else{
                
                thetable=thetable.filter(filter!)
            }
            
            var tb: Table
            
            if orderBy == nil {
                
                tb = thetable
            }else{
                
                tb = thetable.order(orderBy!)
            }
            
            concurrentDBQueue.sync{
                
                do {
                    
                    if table.rawValue == DBTables.Images.rawValue{
                        
                        for i in try db!.prepare(tb){
                            
                            object.append(
                                
                                ImageObject(
                                    product: i[im_product] ?? "",
                                    imageName: i[im_imageName] ?? "",
                                    dateModified: i[im_dateModified] ?? Date(),
                                    image: i[im_image] ?? Data()
                                )
                            )
                        }
                    }
                }catch{
                    
                    print("Failed To Retrieve Image Rows")
                }
            }
        }else{
            
            return ["Invalid Table"]
        }
        
        return object
    }
    
    func tableList() -> [String]{
        
        var tableNames: [String] = []
        
        do{
            
            for p in try self.db!.prepare("SELECT name FROM sqlite_master WHERE type = 'table';"){
                
                tableNames.append(p[0] as! String)
            }
        }catch let error{
            
            print("error \(error)")
        }
        
        return tableNames
    }
}

extension Connection{
    
    func exists(column: String, in table: DBTables) throws -> Bool {
        
        let stmt = try prepare("PRAGMA table_info(\(table.rawValue))")
        
        let columnNames = stmt.makeIterator().map { (row) -> String in
            
            return row[1] as? String ?? ""
        }
        
        return columnNames.contains(where: { dbColumn -> Bool in
            
            return dbColumn.caseInsensitiveCompare(column) == ComparisonResult.orderedSame
        })
    }
}
