//
//  ImageObject.swift
//  SPReceiver
//
//  Created by Will Smith on 16/01/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import Foundation

class ImageObject {
    
    var Product: String = ""
    var ImageName: String = ""
    var DateModified: Date?
    var Image: Data = Data()
    
    init(product: String = "", imageName: String = "", dateModified: Date = Date(), image: Data = Data()){
        
        self.Product = product
        self.ImageName = imageName
        self.DateModified = dateModified
        self.Image = image
    }
    
    deinit {}
}
