//
//  NetworkHandlers.swift
//  SPReceiver
//
//  Created by Will Smith on 15/11/2019.
//  Copyright © 2019 pricecheck.uk.com. All rights reserved.
//

import Foundation
import Alamofire

extension NSLock {
    
    func withCriticalScope<T>( block: () -> T) -> T {
        
        lock()
        let value = block()
        unlock()
        
        return value
    }
}

public class AsynchronousOperation: Operation {
    
    override public var isAsynchronous: Bool { return true }
    
    private let stateLock = NSLock()
    
    private var _executing: Bool = false
    
    override private(set) public var isExecuting: Bool {
        
        get {
            
            return stateLock.withCriticalScope { _executing }
        }
        set {
            
            willChangeValue(forKey: "isExecuting")
            stateLock.withCriticalScope { _executing = newValue }
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private var _finished: Bool = false
    
    override private(set) public var isFinished: Bool {
        
        get {
            
            return stateLock.withCriticalScope { _finished }
        }
        set {
            
            willChangeValue(forKey: "isFinished")
            stateLock.withCriticalScope { _finished = newValue }
            didChangeValue(forKey: "isFinished")
        }
    }
    
    public func completeOperation() {
        if isExecuting {
            
            isExecuting = false
        }
        
        if !isFinished {
            
            isFinished = true
        }
    }
    
    override public func start() {
        
        if isCancelled {
            
            isFinished = true
            return
        }
        
        isExecuting = true
        
        main()
    }
    
    override public func main() {
        
        fatalError("Subclasses Must Override 'Main'")
    }
}

class DownloadImages: AsynchronousOperation {
    
    let URLString: String
    let networkOperationCompletionHandler: (_ responseObject: Data?, _ error: Error?) -> ()
        
    weak var request: Alamofire.Request?
        
    init(URLString: String, networkOperationCompletionHandler: @escaping (_ responseObject: Data?, _ error: Error?) -> ()) {
        
        self.URLString = URLString

        self.networkOperationCompletionHandler = networkOperationCompletionHandler
        super.init()
    }
    
    override func main() {
        
        let downloadQueue = DispatchQueue(label: "com.uk.pricecheck.DownloadImage-queue", qos: .utility, attributes: [.concurrent])
        
        request = Alamofire.request(URLString, method: .get).responseData(queue: downloadQueue){ response in
                
            self.networkOperationCompletionHandler(response.result.value ?? Data(), response.error)
                        
            self.completeOperation()
        }
    }
        
    override func cancel() {
        request?.cancel()
        super.cancel()
    }
}
